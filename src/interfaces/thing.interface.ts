import { Property } from "../models/property";

export interface ThingInterface {
    id: string;
    name: string;
    type: string;
    description: string;
    properties: Property[];
}
