import { Property } from "../models/property";

export interface PropertyInterface {
    id: string;
    name: string;
    type: string;
    description: string;
}
