import {Dht22Model} from "../models/sensors/dht22";

export class Dht22Controller {
    public DHT22;

    constructor() {
        this.createInstance();
    }

    createInstance() {
        this.DHT22 = new Dht22Model(D2);
        this.DHT22.startScan();
    }

    getData() {
        return {
            temperature: this.DHT22.getTemperature(),
            humidity: this.DHT22.getHumidity()
        }
    }


}