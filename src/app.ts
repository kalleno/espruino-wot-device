import {Thing} from "./models/thing";
import {Wlan} from "./providers/wlan"
import {Dht22Controller} from "./controllers/dht22Controller";
import {Dht22Model} from "./models/sensors/dht22";

export class App {

    private wlan: Wlan;
    private http;
    private appConfig;

    private Dht22Ctrl: Dht22Controller;
    private DHT22: any;

    constructor() {
        this.appConfig = require("./app-config");
        this.wlan = new Wlan();
        this.http = require("http");
        // console.log(this.newThing);
        this.http.createServer(this.onPageRequest).listen(80);

        this.DHT22 = new Dht22Model();
        this.DHT22.startScan();

        setInterval(() => {
            console.log(this.DHT22.getData());
        }, 500);
    }

    onPageRequest(req: any, res: any) {
        let things = new Array;
        things.push({
            name: 'DHT22 (Temperature, Humidity)',
            type: 'thing',
            description: 'DHT22 Temperature and Humidity Sensor',
            properties: {
                temperature: {
                    type: 'number',
                    unit: 'celcius',
                    description: 'temperature',
                    href: '/properties/temperature'
                },
                humidity: {
                    type: 'number',
                    unit: 'percent',
                    href: '/properties/humidity'
                },
            },
            events: {
                highHumidity: {
                    type: 'number',
                    unit: 'celcius',
                    description: 'To high humidity detected! ',
                    href: '/things/esp/events/highhumidity'
                }
            },
            links: [{
                'rel': 'properties',
                'href': '/things/esp/properties'
            }]
        });

       // let sensorData = this.Dht22Ctrl.getData();

        var a = url.parse(req.url, true);


        if (a.pathname == "/debug") {
            res.writeHead(200, {'Content-Type': 'text/json', 'Accept': 'text/json'});
            res.end(JSON.stringify('Test 123!!'));
        } else if (a.pathname == "/things/esp") {
            res.writeHead(200, {'Content-Type': 'text/json', 'Accept': 'text/json'});
            res.end(JSON.stringify(things));
        } else if (a.pathname == "/things/esp/properties/temperature") {
            res.writeHead(200, {'Content-Type': 'text/json', 'Accept': 'text/json'});
            res.end(JSON.stringify({temperature: 21}));
        } else if (a.pathname == "/things/esp/properties/humidity") {
            res.writeHead(200, {'Content-Type': 'text/json', 'Accept': 'text/json'});
            res.end(JSON.stringify({humidity: 40}));
        } else if (a.pathname == "/things/esp/events/highhumidity") {
            res.writeHead(200, {'Content-Type': 'text/json', 'Accept': 'text/json'});
            res.end(JSON.stringify([{"overheated": {"data": 102, "timestamp": "2017-01-25T15:01:35+00:00"}}]));
        } else {
            res.writeHead(404, {'Content-Type': 'text/plain'});
            res.end("404: Page " + a.pathname + " not found");
        }
    }

}

// reset();
function onInit() {
    new App();
}

save();