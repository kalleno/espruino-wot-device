export class Wlan {
    private wifi: any = require("Wifi");
    private appConfig;
    
    constructor() {
      this.appConfig = require("./app-config");
      this.initWifi();
    }

    private initWifi() {
        console.log('Init WiFi')
        // this.wifi.stopAP(); // disable Wi-Fi AP
        this.wifi.connect(this.appConfig.default.wifi_ssid, this.appConfig.default.wifi_options, (err: any) =>  {
          if (err) {
            console.log("ERROR: Connection failed, error: " + err);
          } else {
            console.log("INFO: Successfully connected");
            console.log(this.wifi.getStatus());
            console.log(this.wifi.getIP());
            // wifi.setHostname("espruino");
            this.wifi.save();
          }
        });
    }   
}