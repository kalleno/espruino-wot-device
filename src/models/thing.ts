import { Property } from "./property";
import { ThingInterface } from "../interfaces/thing.interface";

export class Thing {
    private properties: Property[] = [];
    
    constructor (
        protected thingOrigin : ThingInterface
    ) {
        for (const property of this.thingOrigin.properties) {
            this.properties.push(property);
        }
    }

    getId() {
        return this.thingOrigin.id;
    }

    getName() {
        return this.thingOrigin.name;
    }

    getProperties() {
        return this.thingOrigin.properties;
    }

}