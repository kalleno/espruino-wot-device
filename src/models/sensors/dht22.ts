export class Dht22Model {
    private dht: any = require("DHT22");


    private temp: any = 0;
    private humidity: any = 0;

    private scanInterval;

    constructor() {
        this.dht.connect(D12)
    }

    startScan(): void {
        this.scanInterval = setInterval(() => {
            this.dht.read(function (a) {
                this.temp = a.temp.toString();
                this.humidity = a.rh.toString();
            });
        }, 500);
    }

    stopScan(): void {
        clearInterval(this.scanInterval);
    }


    getTemperature(): number {
        return this.temp;
    }

    getHumidity(): number {
        return this.humidity;
    }

    getData() {
        return {
            humidity: this.getHumidity(),
            temperature: this.getTemperature()
        }
    }

    connect() {}


}