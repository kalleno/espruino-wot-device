export class Interaction {
    id: number = 0;
    innerRadius: number = 0;
    backgroundClass: string = "";
    backgroundOpacity: number = 0;



    public getUnit() {
        return 
    }

    public getInteraction() {
        let json = {
            "id" : "wohnzimmer",
            "name":"DHT22",
            "type": "multiLevelSensor",
            "description": "DHT Sensor",
            "properties": {
              "temperature": {
                "type": "number",
                "unit": "celsius",
                "description": "An ambient temperature sensor",
                "href": "/things/pi/properties/temperature"
              },
              "humidity": {
                "type": "number",
                "unit": "percent",
                "href": "/things/pi/properties/humidity"
              }
            },
            "actions": {
              "reboot": {
                "description": "Reboot the device"
              }
            },
            "events": {
              "reboot": {
                "description": "Going down for reboot"
              }
            },
            "links": {
              "properties": "/thing/pi/properties",
              "actions": "/things/pi/actions",
              "events": "/things/pi/events",
              "websocket": "wss://mywebthingserver.com/things/pi"
            }
          };          
        return json;
    }
}